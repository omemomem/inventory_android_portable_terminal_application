package com.apps.meirovichomer.inventoryapp.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.apps.meirovichomer.inventoryapp.R;

/**
 * Created by meiro on 12/5/2017.
 */

public class ManualBarcodeDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button confirm;
    public EditText brc;

    public ManualBarcodeDialog(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.manual_barcode_dialog);
        confirm = (Button) findViewById(R.id.barcode_dialog_confirm);
        confirm.setOnClickListener(this);
        brc = (EditText) findViewById(R.id.barcode_dialog_et);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.barcode_dialog_confirm:
                dismiss();
                break;
            default:
                break;
        }
    }
}
