package com.apps.meirovichomer.inventoryapp.classes;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarcodeData {


    /**
     * Properties of a single barcode
     */
    @SerializedName("barcode")
    @Expose
    private String barcode;
    @SerializedName("catalogNumber")
    @Expose
    private String catalogNumber;
    @SerializedName("itemDescription")
    @Expose
    private String itemDescription;
    @SerializedName("amount")
    @Expose
    private Integer amount;


    /**
     * Full constructor for a full barcode scan
     */
    public BarcodeData(String barcode, String catalogNumber, String itemDescription, Integer amount) {

        this.barcode = barcode;
        this.catalogNumber = catalogNumber;
        this.itemDescription = itemDescription;
        this.amount = amount;
    }

    /**
     * Partial constructor for a scan without itemDescription
     */
    public BarcodeData(String barcode, String catalogNumber, Integer amount) {

        this.barcode = barcode;
        this.catalogNumber = catalogNumber;
        this.amount = amount;
        itemDescription = "";
    }

    /**
     * Partial constructor for a scan with a barcode and item count only
     */
    public BarcodeData(String barcode, Integer amount) {

        this.barcode = barcode;
        this.amount = amount;
        this.catalogNumber = "";
        this.itemDescription = "";
    }

    /**
     * Setters/Getters
     */
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setCatalogNumber(String catalogNumber) {
        this.catalogNumber = catalogNumber;
    }

    public String getCatalogNumber() {
        return catalogNumber;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmmount() {
        return amount;
    }

    /**
     * length of barcode/catalogNumber functions
     */
    public int barcodeLength() {
        return barcode.length();
    }

    public int catalogNumberLength() {
        return catalogNumber.length();
    }

    /**
     * Check if barcode/CatNumber/ItemDescription equals to a given String
     */
    public boolean isBarcodeEqual(String barcodeToCheck) {

        return barcode.equals(barcodeToCheck);
    }

    public boolean isCatalogNumEqual(String catalogNumberToCheck) {

        return catalogNumber.equals(catalogNumberToCheck);
    }

    // In this function a partial match is also good
    public boolean isDescriptionEquals(String description) {

        return itemDescription.toLowerCase().contains(description);
    }

    /**
     * Check if given barcodeData object equals to this one
     */
    public boolean isBarcodeDataEquals(String bdata) {

        return isBarcodeEqual(bdata);
    }
}
