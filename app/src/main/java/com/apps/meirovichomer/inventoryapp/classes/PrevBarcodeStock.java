package com.apps.meirovichomer.inventoryapp.classes;

/**
 * Created by meiro on 12/5/2017.
 */

public class PrevBarcodeStock {

    /**
     * This class will hold a key -> count of a barcode and its value
     */

    private String barcode;
    private Integer amount;

    public PrevBarcodeStock(String barcode, Integer amount) {
        this.barcode = barcode;
        this.amount = amount;
    }

    /**
     * Setters/Getters
     */

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    /**
     *
     */

    public boolean isBarcodeDataEquals(String barcode) {

        return this.barcode.equals(barcode);
    }
}
