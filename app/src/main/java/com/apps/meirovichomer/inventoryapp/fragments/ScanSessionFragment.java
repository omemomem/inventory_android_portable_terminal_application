package com.apps.meirovichomer.inventoryapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.meirovichomer.inventoryapp.R;
import com.apps.meirovichomer.inventoryapp.classes.BarcodeData;
import com.apps.meirovichomer.inventoryapp.classes.ScanSessionClass;
import com.apps.meirovichomer.inventoryapp.dialogs.ManualBarcodeDialog;
import com.apps.meirovichomer.inventoryapp.dialogs.ManualCatalogDialog;
import com.google.gson.Gson;
import com.honeywell.scanintent.ScanIntent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.honeywell.scanintent.ScanIntent.EXTRA_RESULT_BARCODE_DATA;
import static com.honeywell.scanintent.ScanIntent.SCAN_MODE_RESULT_AS_URI;
import static com.honeywell.scanintent.ScanIntent.SCAN_RESULT_SUCCESSED;


public class ScanSessionFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    /**
     * ScanSession variables
     */
    ScanSessionClass currentSession;

    SharedPreferences sharedBuild;
    SharedPreferences sharedPreviousArray;
    SharedPreferences sharedCurrentArray;
    Integer buildDigit;
    ManualBarcodeDialog manualBarcodeDia;
    ManualCatalogDialog manualCatalogDia;
    TextView brcTextView;
    TextView currentCount;
    TextView previousCount;
    TextView countDifference;
    TextView catalogTextView;
    EditText amountEditText;
    EditText itemDesciptionText;

    boolean FLAG_SCAN_CLICKED = false;
    boolean FLAG_AUTO_SCAN = false;

    private OnFragmentInteractionListener mListener;

    public ScanSessionFragment() {
        // Required empty public constructor
    }

    public static ScanSessionFragment newInstance(String param1, String param2) {
        ScanSessionFragment fragment = new ScanSessionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scan_session, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        arraysToSharedPrefernces();
        Toast.makeText(getActivity(), "Going out!", Toast.LENGTH_SHORT).show();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    // Overrides onActivityResult in order to get the barcode data.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Manage barcode data according to the device
        switch (buildDigit) {
            case 1:
                if (requestCode == 5) {
                    // Check if result has data in it
                    if (resultCode == SCAN_RESULT_SUCCESSED && data != null) {

                        String barcodeResult = data.getStringExtra(EXTRA_RESULT_BARCODE_DATA);

                        // TODO add option to toggle automatic scan.
                        // If auto scan is on, don't set UI and automatically add the item to the array.
                        if (FLAG_AUTO_SCAN) {
                            brcTextView = getActivity().findViewById(R.id.barcode_et);
                            brcTextView.setText(barcodeResult);
                            addBarcodeDataToArray();
                        } else {
                            // get barcode from result and set the UI with barcode
                            setBarcodeUi(barcodeResult);
                        }
                    }
                }
                break;
        }

        // Flag clicked off so user can scan again
        FLAG_SCAN_CLICKED = false;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setCurrentSessionClass();
        getDeviceName();
        clickEvents();
    }

    // Get data from shared prefs if there is any if not start new.
    private void setCurrentSessionClass() {

        currentSession = new ScanSessionClass();

        sharedPreviousArray = getActivity().getSharedPreferences(getString(R.string.shared_previousArr), Context.MODE_PRIVATE);
        sharedCurrentArray = getActivity().getSharedPreferences(getString(R.string.shared_currentArr), Context.MODE_PRIVATE);


        // Get the raw JSON string and 'Strip' it to get the values only, Then run a loop and take the values and insert them into the
        // currentSession array
        try {
            String currentJson = sharedCurrentArray.getString("array", "");
            // In order to get only the values of the inserted array we need to pour the data into a new string
            JSONObject currentStrippedJson = new JSONObject(currentJson);
            String currentStrippedString = currentStrippedJson.getString("values");
            // Insert the strippedString into a JSONArray
            JSONArray currentJsonArray = new JSONArray(currentStrippedString);
            // Run a loop on the jsonArrays length
            for (int i = 0; i < currentJsonArray.length(); i++) {

                // IN CASE OF PROPERTIES CHANGE, @ScanSessionClass should be changed here to handle new properties
                JSONObject tmpObj = currentJsonArray.getJSONObject(i);
                Integer amount = tmpObj.getInt("amount");
                String barcode = tmpObj.getString("barcode");
                String catNum = tmpObj.getString("catalogNumber");
                String itemDes = tmpObj.getString("itemDescription");
                // Take the data of the current(i) item and into a temp BarcodeData object, then insert to the currentSession Array
                BarcodeData tmp = new BarcodeData(barcode, catNum, itemDes, amount);
                currentSession.addBarcodeItem(tmp);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Get the raw JSON string and 'Strip' it to get the values only, Then run a loop and take the values and insert them into the
        // currentSession array
        // TODO HANDLE PREVIOUS ARRAY.
//        try {
//            String previousJson = sharedPreviousArray.getString("array", "");
//            // In order to get only the values of the inserted array we need to pour the data into a new string
//            JSONObject prevStrippedJson = new JSONObject(previousJson);
//            String prevStrippedString = prevStrippedJson.getString("values");
//            // Insert the strippedString into a JSONArray
//            JSONArray prevJsonArray = new JSONArray(prevStrippedString);
//            // Run a loop on the jsonArrays length
//            for (int i = 0; i < prevJsonArray.length(); i++) {
//
//                // IN CASE OF PROPERTIES CHANGE, @ScanSessionClass should be changed here to handle new properties
//                JSONObject tmpPrevObj = prevJsonArray.getJSONObject(i);
//                Integer amountP = tmpPrevObj.getInt("amount");
//                String barcodeP = tmpPrevObj.getString("barcode");
//                String catNumP = tmpPrevObj.getString("catalogNumber");
//                String itemDesP = tmpPrevObj.getString("itemDescription");
//                // Take the data of the current(i) item and into a temp BarcodeData object, then insert to the currentSession Array
//                BarcodeData tmp = new BarcodeData(barcodeP, catNumP, itemDesP, amountP);
//                currentSession.addBarcodeItem(tmp);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }

    //TODO add condition to make sure arrays are not empty
    // Get current array data, parse to JSON and insert the JSON string into shared prefs.
    private void arraysToSharedPrefernces() {

        sharedPreviousArray = getActivity().getSharedPreferences(getString(R.string.shared_previousArr), Context.MODE_PRIVATE);
        sharedCurrentArray = getActivity().getSharedPreferences(getString(R.string.shared_currentArr), Context.MODE_PRIVATE);

        Gson gson = new Gson();

        // Current session to sharedPrefs
        // Create a JSONArray object
        JSONArray jsonCurrentArray = new JSONArray();

        // Insert the JSONArray every BarcodeData item
        for (int i = 0; i < currentSession.getBarcodesArray().size(); i++) {
            jsonCurrentArray.put(currentSession.getBarcodesArray().get(i));
        }

        //Convert the JSONArray to JSON using gson and apply it to the shared preferences.
        SharedPreferences.Editor editorCurrent = sharedCurrentArray.edit();
        editorCurrent.putString("array", gson.toJson(jsonCurrentArray));
        editorCurrent.apply();

        // Previous session to SharedPrefs

        JSONArray jsonPreviousArray = new JSONArray();

        for (int i = 0; i < currentSession.getBarcodesArray().size(); i++) {

            jsonPreviousArray.put(currentSession.getBarcodesArray().get(i));
        }

        //SAVE NEW ARRAY
        SharedPreferences.Editor editorPrev = sharedCurrentArray.edit();
        editorPrev.putString("array", gson.toJson(jsonPreviousArray));
        editorPrev.apply();


    }

    // Arrange the previous session ArrayList<Barcode> into an ArrayList<PrevBarcodeStock> arranged array.
    private void arrangePreviousArray(ArrayList<BarcodeData> prevArray) {
        //TODO ARRANGE GIVEN ARRAY AND INSERT IT TO THE SCANSESSIONCLASS Prevoius array.
    }

    private void clickEvents() {

        Button scanBarcode = getActivity().findViewById(R.id.scanBarcode_btn);
        Button manualBarcode = getActivity().findViewById(R.id.manualBarcode_btn);
        Button manualCatalog = getActivity().findViewById(R.id.manualCatalog_btn);
        Button addItemBtn = getActivity().findViewById(R.id.insertItem_btn);


        // Start a scan
        scanBarcode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO add datalogic function

                if (!FLAG_SCAN_CLICKED) {
                    FLAG_SCAN_CLICKED = true;
                    if (buildDigit == 1) {
                        honeywellScan();
                    }
                }

            }
        });

        // Manually add a barcode, open dialog then take value from EditText into the TextView barcode
        manualBarcode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                manualBarcodeDia = new ManualBarcodeDialog(getActivity());
                manualBarcodeDia.show();
                // Define the confirm button onClick listener
                manualBarcodeDia.confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // Take value from EditText
                        String tempBrc = manualBarcodeDia.brc.getText().toString();

                        if (!isNummbersOnly(tempBrc)) {
                            Toast.makeText(getActivity(), "אנא וודא שהכנסת מספר", Toast.LENGTH_SHORT).show();
                        } else if (tempBrc.length() < 1) {
                            Toast.makeText(getActivity(), "לא הוכנס ברקוד", Toast.LENGTH_SHORT).show();
                        } else {
                            setBarcodeUi(tempBrc);
                            manualBarcodeDia.dismiss();
                        }
                    }
                });
            }
        });

        // Manually add a catalog number, Open dialog take value from EditText into TextView barcode.
        manualCatalog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                manualCatalogDia = new ManualCatalogDialog(getActivity());
                manualCatalogDia.show();
                // Define the confirm button onClick listener
                manualCatalogDia.confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // Take value from EditText
                        String tempBrc = manualCatalogDia.brc.getText().toString();

                        if (!isNummbersOnly(tempBrc)) {
                            Toast.makeText(getActivity(), "אנא וודא שהכנסת מספר", Toast.LENGTH_SHORT).show();
                        } else if (tempBrc.length() < 1) {
                            Toast.makeText(getActivity(), "לא הוכנס ברקוד", Toast.LENGTH_SHORT).show();
                        } else {
                            TextView catTextView = getActivity().findViewById(R.id.catalogItem_et);
                            catTextView.setText(tempBrc);
                            manualCatalogDia.dismiss();
                        }
                    }
                });
            }
        });

        // Add a new item click listener
        addItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBarcodeDataToArray();
            }
        });
    }

    // TODO get barcode check stock and change UI accordingly also change TextView to show barcode
    private void setBarcodeUi(String barcode) {

        brcTextView = getActivity().findViewById(R.id.barcode_et);
        currentCount = getActivity().findViewById(R.id.currentItemCount_txt);
        previousCount = getActivity().findViewById(R.id.prevItemCount_txt);
        countDifference = getActivity().findViewById(R.id.differenceItemCount_txt);

        Integer diffInt = currentSession.getItemCountDifference(barcode);
        // Convert the Integer counts to string
        String currentString = currentSession.getBarcodesItemCount(barcode).toString();
        String prevCount = currentSession.getPreviousItemCount(barcode).toString();
        String diffCount = diffInt.toString();

        // Set converted Integer count string to the TextViews Text
        try {
            // Set counts txts
            //TODO PROBLEM HERE!
            brcTextView.setText(barcode);
            currentCount.setText(currentString);
            previousCount.setText(prevCount);
            countDifference.setText(diffCount);
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }

        // Color Difference TextView
        if (diffInt > 0) {
            countDifference.setBackgroundColor(Color.GREEN);
        } else if (diffInt < 0) {
            countDifference.setBackgroundColor(Color.RED);
        } else {
            countDifference.setBackgroundColor(Color.WHITE);
        }

    }

    // Get all current data from Fragment, Add it to a BarcodeData object and add to the currentSession Array.
    private void addBarcodeDataToArray() {

        // TODO confirm data INT HAS TO BE INT
        brcTextView = getActivity().findViewById(R.id.barcode_et);
        catalogTextView = getActivity().findViewById(R.id.catalogItem_et);
        amountEditText = getActivity().findViewById(R.id.amount_et);
        itemDesciptionText = getActivity().findViewById(R.id.itemDescription_et);

        // Check is barcode field filled.
        if (!(Integer.parseInt(brcTextView.getText().toString()) == 0)) {
            try {
                BarcodeData item = new BarcodeData(brcTextView.getText().toString(), catalogTextView.getText().toString(), itemDesciptionText.getText().toString(), Integer.parseInt(amountEditText.getText().toString()));
                currentSession.addBarcodeItem(item);
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
            // After adding an item clear all fields
            clearFields();

            Toast.makeText(getActivity(), "מוצר נוסף בהצלחה", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "אנא וודא כי הוכנס ברקוד", Toast.LENGTH_SHORT).show();
        }
    }

    // Clear all data fields in the current fragment
    private void clearFields() {

        brcTextView = getActivity().findViewById(R.id.barcode_et);
        catalogTextView = getActivity().findViewById(R.id.catalogItem_et);
        amountEditText = getActivity().findViewById(R.id.amount_et);
        itemDesciptionText = getActivity().findViewById(R.id.itemDescription_et);
        currentCount = getActivity().findViewById(R.id.currentItemCount_txt);
        previousCount = getActivity().findViewById(R.id.prevItemCount_txt);
        countDifference = getActivity().findViewById(R.id.differenceItemCount_txt);

        brcTextView.setText("00000000");
        catalogTextView.setText("00000000");
        amountEditText.setText("1");
        itemDesciptionText.setText("");
        currentCount.setText("0");
        previousCount.setText("0");
        countDifference.setText("0");
        countDifference.setBackgroundColor(Color.WHITE);
    }

    // Scan a barcode using built-in honeywell scanner
    private void honeywellScan() {

        //SCAN_MODE_SHOW_RESULT_UI - OPEN UI WINDOW WITH RESULTS
        //SCAN_MODE_RESULT_AS_URI - ONACTIVITYRESULT URI
        Intent intentScan = new Intent(ScanIntent.SCAN_ACTION);
        intentScan.putExtra("scan_mode", SCAN_MODE_RESULT_AS_URI);
        startActivityForResult(intentScan, SCAN_MODE_RESULT_AS_URI);
    }

    // Get the device name using sharedPreferences
    private void getDeviceName() {

        sharedBuild = getActivity().getSharedPreferences(getString(R.string.shared_build), Context.MODE_PRIVATE);
        buildDigit = sharedBuild.getInt("build", 0);
    }

    // Check if a string countains numbers only
    private boolean isNummbersOnly(String stCheck) {

        return stCheck.matches("[0-9]+");

    }

}
