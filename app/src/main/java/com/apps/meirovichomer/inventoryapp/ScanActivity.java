package com.apps.meirovichomer.inventoryapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.apps.meirovichomer.inventoryapp.fragments.ScanSessionFragment;

public class ScanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ScanSessionFragment fragment = new ScanSessionFragment();
        fragmentTransaction.add(R.id.scanFragment, fragment);
        fragmentTransaction.commit();

    }
}
