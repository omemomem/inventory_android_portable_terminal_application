package com.apps.meirovichomer.inventoryapp.classes;

import java.util.ArrayList;

public class ScanSessionClass {

    /**
     * Properties of an entire scan session
     */
    private String warehouseNumber;
    private String warehouseDescription;
    private String shelfDescription;
    private ArrayList<BarcodeData> barcodesArray;
    private ArrayList<PrevBarcodeStock> previousSessionArray;

    /**
     * Default constructor
     */
    public ScanSessionClass() {
        warehouseNumber = "";
        warehouseDescription = "";
        shelfDescription = "";
        barcodesArray = new ArrayList<BarcodeData>();
        previousSessionArray = new ArrayList<PrevBarcodeStock>();
    }

    /**
     * Full constructor
     */
    public ScanSessionClass(String warehouseNumber, String warehouseDescription, String shelfDescription) {
        this.warehouseNumber = warehouseNumber;
        this.warehouseDescription = warehouseDescription;
        this.shelfDescription = shelfDescription;
        barcodesArray = new ArrayList<BarcodeData>();
        previousSessionArray = new ArrayList<PrevBarcodeStock>();
    }

    /**
     * Setters/Getters
     */

    public void setWarehouseNumber(String warehouseNumber) {
        this.warehouseNumber = warehouseNumber;
    }

    public String getWarehouseNumber() {
        return warehouseNumber;
    }

    public void setWarehouseDescription(String warehouseDescription) {
        this.warehouseDescription = warehouseDescription;
    }

    public String getWarehouseDescription() {
        return warehouseDescription;
    }

    public void setBarcodesArray(ArrayList<BarcodeData> barcodesArray) {
        this.barcodesArray = new ArrayList<>(barcodesArray);
    }

    public ArrayList<BarcodeData> getBarcodesArray() {
        return barcodesArray;
    }

    public void setShelfDescription(String shelfDescription) {
        this.shelfDescription = shelfDescription;
    }

    public String getShelfDescription() {
        return shelfDescription;
    }

    public void setPreviousSessionArray(ArrayList<PrevBarcodeStock> previousSessionArray) {
        this.previousSessionArray = new ArrayList<>(previousSessionArray);
    }

    public ArrayList<PrevBarcodeStock> getPreviousSessionArray() {
        return previousSessionArray;
    }


    /**
     * Add a barcode item to the array
     */
    public void addBarcodeItem(BarcodeData bdata) {

        barcodesArray.add(bdata);
    }

    /**
     * Get barcodesArray length
     */
    public Integer barcodesArraySize() {
        return barcodesArray.size();
    }

    /**
     * Check if barcodeArray is empty
     */
    public boolean isBarcodeArrayEmpty() {
        return barcodesArray.isEmpty();
    }

    /**
     * Remove item from barcodeArray
     */
    public void removeBarcodesItem(Integer itemIndex) {

        barcodesArray.remove(itemIndex.intValue());
    }

    /**
     * Get count of an item in the barcodeArray
     */
    public Integer getBarcodesItemCount(String bDataCheck) {

        if (barcodesArray.isEmpty()) {
            return 0;
        }

        Integer currentCount = 0;
        for (int i = 0; i < barcodesArraySize(); i++) {

            if (barcodesArray.get(i).isBarcodeDataEquals(bDataCheck)) {
                currentCount = currentCount + barcodesArray.get(i).getAmmount();
            }
        }
        return currentCount;
    }

    /**
     * Get count of an item from the previousSessionArray
     */
    public Integer getPreviousItemCount(String bDataCheck) {

        if (previousSessionArray.isEmpty()) {
            return 0;
        }

        Integer currentCount = 0;
        for (int i = 0; i < barcodesArraySize(); i++) {

            if (previousSessionArray.get(i).isBarcodeDataEquals(bDataCheck)) {
                currentCount = currentCount + previousSessionArray.get(i).getAmount();
            }
        }
        return currentCount;
    }

    /**
     * Get the count difference
     */
    public Integer getItemCountDifference(String bdataCheck) {

        return getBarcodesItemCount(bdataCheck) - getPreviousItemCount(bdataCheck);
    }
}
