package com.apps.meirovichomer.inventoryapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSharedPrefDeviceName();
        clickEvents();
    }

    private void clickEvents() {

        Button toScanSession = findViewById(R.id.toScanSession_btn);
        toScanSession.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                startActivity(intent);
            }
        });
    }

    // Check device build name and add to shared prefs a reference to the device name
    private void setSharedPrefDeviceName() {

        SharedPreferences sharedBuild = getSharedPreferences(getString(R.string.shared_build), Context.MODE_PRIVATE);

        if (isDolphinDevice()) {

            SharedPreferences.Editor editor = sharedBuild.edit();
            editor.putInt("build", 1);
            editor.commit();

        } else if (false) {
            //TODO CHECK DATALOGIC DEVICE
        } else {
            SharedPreferences.Editor editor = sharedBuild.edit();
            editor.putInt("build", 0);
            editor.commit();
        }

    }

    public static boolean isDolphinDevice() {
        return Build.MODEL.toLowerCase().contains("dolphin");
    }
}
